<?php

require_once __DIR__ . '/../vendor/autoload.php';


function get_lead_id($input){
  return $input['entry'][0]['changes'][0]['value']['leadgen_id'];
}

function get_page_id($input){
  return $input['entry'][0]['changes'][0]['value']['page_id'];
}

function get_lead_data_array($lead_data_field){

  $lead=array();

  foreach($lead_data_field->field_data as $data_lead){
  if($data_lead['name']==="full_name"){
    $lead['Nombre']=$data_lead['values'][0];
  }
  else if($data_lead['name']==="email"){
    $lead['Email']=$data_lead['values'][0];
  }
  else if($data_lead['name']==="city"){
    $lead['Ciudad']=$data_lead['values'][0];
  }
  else if($data_lead['name']==="phone_number"){
    $lead['Teléfono']=$data_lead['values'][0];
  }
  else if($data_lead['name']==="country"){
    $lead['País']=$data_lead['values'][0];
  }
  else{
  /*
  if(preg_match("/(ayuda|consulta|problema|mensaje|help)/i",$data_lead['name'])){
    $lead['descripcion']=$data_lead['values'][0];
  }
  */
    $lead[ucfirst(str_replace('_',' ',$data_lead['name']))]=$data_lead['values'][0];
  }

  }

  return $lead;
}


function get_template_engine(){
  $loader = new Twig_Loader_Filesystem(__DIR__ . '/../templates/');
  $twig = new Twig_Environment($loader, array(
    'cache' => __DIR__ . '/../cache/',
  ));

  return $twig;
}

function get_email_sender_instance(){

    // configuration
    $config = new Noodlehaus\Config('../config.ini');

    $smtp_host = $config->get('smtp_host');
    $smtp_secure_protocol = $config->get('smtp_secure_protocol');
    $smtp_port = $config->get('smtp_port');
    $smtp_username = $config->get('smtp_username');
    $smtp_password = $config->get('smtp_password');

  $mail = new PHPMailer;

  $mail->isSMTP();

  //$mail->SMTPDebug = 3;

  $mail->Host = $smtp_host;

  $mail->Port = $smtp_port;

  $mail->SMTPSecure = $smtp_secure_protocol;

  $mail->SMTPAuth = true;

  $mail->Username = $smtp_username;

  $mail->Password = $smtp_password;

  $mail->setFrom($smtp_username,'Notificador Facebook Leads Artesyweb');

  $mail->isHTML(true);

  $mail->CharSet = "UTF-8";

  return $mail;

}

function send_email_notification($address, $subject, $lead, $company){
  $mail=get_email_sender_instance();

  foreach(explode(',',$company) as $email){
    $mail->addAddress($email);
  }

  foreach(explode(',',$address) as $email){
    $mail->addAddress($email);
  }

  $mail->Subject = $subject;

  $te = get_template_engine();


  $mail->Body = $te->render('standar.html', array('campos'=>$lead));

  if(!$mail->send()){
    error_log(sprintf("No se pudo enviar el correo de notificacion del cliente %s -> %s",$lead['Email'],$mail->ErrorInfo));
  }
  else{
   error_log(sprintf("Se ha enviado una notificacion del cliente %s a %s",$lead['Email'],$subject));
  }


}

function get_database_instance(){
    $config = new Noodlehaus\Config('../config.ini');

    $db_host = $config->get('db_host');
    $db_username = $config->get('db_username');
    $db_password = $config->get('db_password');
    $db_database = $config->get('db_database');

  $mysqli = new mysqli($db_host,$db_username,$db_password,$db_database);
  return $mysqli;
}

?>
