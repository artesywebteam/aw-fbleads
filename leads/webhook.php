<?php

require_once __DIR__ . '/../include/leads-notifier-functions.php';
require_once __DIR__ . '/../include/facebook-functions.php';
require_once __DIR__ . '/../vendor/autoload.php';



// configuration
$config = new Noodlehaus\Config('../config.ini');

$app_id = $config->get('facebook_app_id');
$app_secret = $config->get('facebook_app_secret');
$access_token = $config->get('facebook_user_access_token');
$verify_token = $config->get('facebook_verify_token');

$sentry_dsn = $config->get('sentry_dsn');
$sentry_client = new Raven_Client($sentry_dsn);
$sentry_client->install();

// Verifying the verify_token ensures nobody else can
// subscribe random things to your application.

if (isset($_REQUEST['hub_verify_token'])) {
    if ($_REQUEST['hub_verify_token'] === $verify_token) {
        echo $_REQUEST['hub_challenge'];
    }
    else{
    // exit;ALdzziCOnkBANZAVk5CWZBmXCbklUDNuGp6ZBlg49Yj5ZCRZAcklYbvnSYZBzHQWiId3rGPSPVNassHQTZBtq3IIUf2AdNaJPJO4nCwoxtYJhZBAVi2Lp0dnCjwt2Ks99QiMJhL3AIsV62gPb133yuDermLjLhMVaYZD"
    }
}

$input = json_decode(file_get_contents('php://input'), true);

foreach($input['entry'] as $entry){
    foreach($entry['changes'] as $change){

        $lead_id = $change['value']['leadgen_id'];
        $page_id = $change['value']['page_id'];


        if(is_null($lead_id) or is_null($page_id)){
            error_log("no existe el cliente potencial");
        }
        else{

            try{

                $data=fb_get_data_lead(
                    $lead_id,
                    //App ID
                    $app_id,
                    //App Secret
                    $app_secret,
                    //Access_token
                    $access_token
                );

                $mysql = get_database_instance();
                if(!$result = $mysql->query("select * from lead_notifier_client where page_id = $page_id limit 1")){
                    throw new Exception("La consulta de la pagina $page_id a la base de datos ha fallado!");
                }
                if($result->num_rows === 0){
                    throw new Exception("No se ha encontrado la pagina $page_id en la base de datos");
                }

                $page = $result->fetch_assoc();

            }
            catch(Exception $e){
                error_log("id pagina: $page_id <br/> id lead $lead_id <br/> Detalles de error: <br/> " . $e->getCode() . " : " . $e->getMessage() . " : " . $e->getFile() . ":" . $e->getLine());
                $sentry_client->captureException($e, [
                    'extra' =>  [
                        'page_id'   =>  $page_id,
                        'lead_id'   =>  $lead_id
                    ],
                    'level' =>  'fatal'
                ]);

                exit;
            }

            $lead = get_lead_data_array($data);

            // correo del cliente a enviar las notificaciones
            send_email_notification($page['email'], "Formulario lead ".$page['name'], $lead, $page['empresa']);
            //send_email_notification($page['email'], "Formulario lead ".$page['name']);

        }

    }
}
